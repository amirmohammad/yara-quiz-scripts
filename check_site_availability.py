import requests

def check_availability(url, timeout=5):
    try:
        req = requests.head(url, timeout=timeout)
        req.raise_for_status()
        print(f"{url} is available.")
        return True
    except requests.HTTPError as e:
        print("Checking internet connection failed, status code {0}.".format(e.response.status_code))
        return False
    except requests.ConnectionError:
        print("No internet connection available.")
    return False
    

check_availability(url="https://varzesh3.com", timeout=3)